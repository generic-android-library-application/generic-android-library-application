package net.coffeecode.gala;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class GALAWebView extends Activity {
	WebView galaWebView;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);
		
		galaWebView = (WebView) findViewById(R.id.webview);
		
		// Make this work like a normal browser
	    galaWebView.getSettings().setJavaScriptEnabled(true);
	    galaWebView.setInitialScale(1);
	    galaWebView.getSettings().setBuiltInZoomControls(true);
	    galaWebView.getSettings().setUseWideViewPort(true);
	    
	    galaWebView.setWebViewClient(new GALAWebViewClient());
	    Intent intent = getIntent();
	    galaWebView.loadUrl(intent.getStringExtra("net.coffeecode.gala.url")); 
	}
	
    private class GALAWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}