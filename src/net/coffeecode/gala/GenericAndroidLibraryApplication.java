package net.coffeecode.gala;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class GenericAndroidLibraryApplication extends ListActivity implements OnSharedPreferenceChangeListener {
	
	private SharedPreferences prefs;
	private String searchUri;
	private String myAccountUri;
	private String latestAddsUri;
	private String hoursUri;
	private String contactUri;
	private String locationUri;
	private String newsUri;
	private String policiesUri;

	public class MainAction {
		@SuppressWarnings("rawtypes")
		Class action;
		String extra_label;
		String extra_value;
		
		public MainAction(Class<?> foo, String bar, String zinga) {
			action = foo;
			extra_label = bar;
			extra_value = zinga;
		}
	};
	                     
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup preferences
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		prefs.registerOnSharedPreferenceChangeListener(this);
		
		final String[] main_menu_text = {
				getResources().getString(R.string.search),
				getResources().getString(R.string.my_account),
				getResources().getString(R.string.latest_adds),
				getResources().getString(R.string.hours),
				getResources().getString(R.string.contact),
				getResources().getString(R.string.location),
				getResources().getString(R.string.news),
				getResources().getString(R.string.policies)
		};
		

		// Set our search URI preference
		searchUri = prefs.getString("search_url", getResources().getString(R.string.search_default));
		myAccountUri = prefs.getString("my_account", getResources().getString(R.string.my_account_default));
		latestAddsUri = prefs.getString("latest_adds", getResources().getString(R.string.latest_adds_default));
		hoursUri = prefs.getString("hours", getResources().getString(R.string.hours_default));
		contactUri = prefs.getString("contact", getResources().getString(R.string.contact_default));
		locationUri = prefs.getString("location", getResources().getString(R.string.location_default));
		newsUri = prefs.getString("news", getResources().getString(R.string.news_default));
		policiesUri = prefs.getString("policies", getResources().getString(R.string.policies_default));

		
		final MainAction[] main_actions = {
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", searchUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", myAccountUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", latestAddsUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", hoursUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", contactUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", locationUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", newsUri),
				new MainAction(GALAWebView.class, "net.coffeecode.gala.url", policiesUri)
		};
		
        setListAdapter(new ArrayAdapter<String>(this, R.layout.main_menu, main_menu_text));

        ListView lv = getListView();
        lv.setTextFilterEnabled(true);

        lv.setOnItemClickListener(new OnItemClickListener() {
        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        		// When clicked, show a toast with the TextView text
//        		Toast.makeText(getApplicationContext(), ((TextView) view).getText(),
//        				Toast.LENGTH_SHORT).show();
        		Toast.makeText(getApplicationContext(), main_actions[position].extra_value, Toast.LENGTH_SHORT).show();
        		Intent intent = new Intent(view.getContext(), main_actions[position].action);
        		intent.putExtra("net.coffeecode.gala.url", main_actions[position].extra_value);
        		startActivity(intent);
        	}
        });

    }
    
    // Called first time user clicks on the menu button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater(); 
    	inflater.inflate(R.menu.main, menu); 
    	return true;
    }

    // Called when an options item is clicked
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    	case R.id.itemPrefs:
    		startActivity(new Intent(this, Preferences.class));
    		break;
    	}

    	return true;
    }


	public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

}

